import React, { Component } from 'react'
import { BrowserRouter, Route } from 'react-router-dom'

import Calculator from './components/calculator'

class Routes extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <div>
            <Route path="/" component={Calculator} exact />
          </div>
        </BrowserRouter>
      </div>
    )
  }
}

export default Routes
