import { combineReducers } from 'redux'

import calculator from './components/calculator/reducer'

const reducers = {
  calculator
}

export default combineReducers(reducers)
