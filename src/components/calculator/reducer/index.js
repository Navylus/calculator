import { fromJS } from 'immutable'
import initialState from './initial-state'
import actionType from '../actions/actions-type'
import '../actions'

const renderOperation = (state, action) => (
  fromJS(state)
    .setIn(['number1'], action.number1)
    .setIn(['number2'], action.number2)
    .toJS()
)

const ajouterNombre = (state, action) => (
  fromJS(state)
    .setIn(['number1'], action.number1)
    .toJS()
)

const changeOperation = (state, action) => (
  fromJS(state)
    .setIn(['operation'], action.operation)
    .setIn(['number2'], action.number1)
    .setIn(['number1'], 0)
    .toJS()
)

const calculator = (state = initialState, action) => {
  switch (action.type) {
    case actionType.RENDER_OPERATION:
      return renderOperation(state, action)
    case actionType.CHANGE_OPERATION:
      return changeOperation(state, action)
    case actionType.ADD_NUMBER:
      return ajouterNombre(state, action)
    default:
      return state
  }
}

export default calculator
