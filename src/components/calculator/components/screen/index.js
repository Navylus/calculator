import React, { Component } from 'react'
import { connect } from 'react-redux'

class Screen extends Component {
  render() {
    const { calculator } = this.props
    if (calculator.number1 === 0) {
      return (
        <div>
          <p>_</p>
        </div>
      )
    }
    return (
      <div>
        <p>
          {`${calculator.number1}`}
        </p>
      </div>
    )
  }
}

export default connect(state => state)(Screen)
