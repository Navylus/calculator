import React, { Component } from 'react'
import { connect } from 'react-redux'
import { setOperation, addNumber } from '../../actions'

class Buttons extends Component {
  constructor(props) {
    super(props)
    const { dispatch } = this.props
    this.dispatch = dispatch
  }

  setNumber(button) {
    const { calculator } = this.props
    this.dispatch(addNumber(button, calculator.number1))
  }

  setOperation(button) {
    const { calculator } = this.props
    this.dispatch(setOperation(button, calculator.number1))
  }

  render() {
    const buttonsList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    const operationsList = ['+', '-', '*', '/']
    return (
      <div>
        <ul className="btn-group col-6">
          { buttonsList.map(button => (
            <li
              key={button}
              onClick={() => this.setNumber(button)}
              className="btn btn-secondary btn-lg active button d-inline  col-1"
            >
              {button}
            </li>
          ))}
        </ul>
        <ul className="btn-group">
          { operationsList.map(button => (
            <li
              key={button}
              onClick={() => this.setOperation(button)}
              className="btn btn-secondary btn-lg active button d-inline  col-1 btn-lg"
            >
              {button}
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

export default connect(state => state)(Buttons)
