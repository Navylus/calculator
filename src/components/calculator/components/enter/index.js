import React, { Component } from 'react'
import { connect } from 'react-redux'
import { doOperation } from '../../actions'

class Enter extends Component {
  constructor(props) {
    super(props)

    const { dispatch } = this.props
    this.dispatch = dispatch
  }

  pressEnter(e) {
    e.preventDefault()
    const { calculator } = this.props
    if (calculator.number1 !== 0) {
      this.dispatch(doOperation(calculator.operation, calculator.number1, calculator.number2))
    }
  }

  render() {
    return (
      <div>
        <button className="btn btn-primary" type="submit" onClick={e => this.pressEnter(e)}>ENTER</button>
      </div>
    )
  }
}

export default connect(state => state)(Enter)
