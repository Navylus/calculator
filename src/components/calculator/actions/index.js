import actionsType from './actions-type'

export const setOperation = (operation, number1) => ({
  type: actionsType.CHANGE_OPERATION,
  operation,
  number1
})

export const addNumber = (buttonNumber, number1) => {
  if (number1 === 0) {
    return ({
      type: actionsType.ADD_NUMBER,
      number1: String(buttonNumber)
    })
  }
  return ({
    type: actionsType.ADD_NUMBER,
    number1: String(number1) + String(buttonNumber)
  })
}

const renderOperation = number1 => ({
  type: actionsType.RENDER_OPERATION,
  number1,
  number2: 0
})

export const doOperation = (sign, number1, number2) => {
  let sum = 0
  switch (sign) {
    case '+':
      sum = parseInt(number2, 10) + parseInt(number1, 10)
      return renderOperation(sum)
    case '-':
      sum = parseInt(number2, 10) - parseInt(number1, 10)
      return renderOperation(sum)
    case '/':
      sum = parseInt(number2, 10) / parseInt(number1, 10)
      return renderOperation(sum)
    case '*':
      sum = parseInt(number2, 10) * parseInt(number1, 10)
      return renderOperation(sum)
    default:
      return renderOperation(sum)
  }
}
