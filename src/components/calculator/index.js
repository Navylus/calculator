import React from 'react'

import Screen from './components/screen'
import Enter from './components/enter'
import Buttons from './components/buttons'

const search = () => (
  <div>
    <Screen />
    <Buttons />
    <Enter />
  </div>
)

export default search
